const cN = require("./connection");
const express = require("express");
const rout = express.Router();
rout.use(express.json())

rout.get("/get", (req, res) => {

    qry = "select * from user";
    cN.query(qry, (err, result) => {

        if (err) {
            return res.send(err)
        }
        user_detail_list = []
        for (item of result)
        {
            let user_detail =
            {
                "user_id": item.user_id,
                "user_name": item.user_name,
                "email": item.email,
                "mobile_number": item.mobile_number

            }
            user_detail_list.push(user_detail)
        }

        res.send(user_detail_list)
        
    }

    )
})

rout.get("/get/:user_id", (req, res) => {


    qry = `select * from user where user_id=?`
    cN.query(qry,req.params.user_id, (err, result) => {
        if (err) {
            return res.send(err)
        }
        if (result.length==0)
        {
            return res.status(400).send("this user id not exist in database")
        }
        let user_detail =
        {
            "user_id": result[0].user_id,
            "user_name": result[0].user_name,
            "email": result[0].email,
            "mobile_number": result[0].mobile_number

        }
        return res.send(user_detail)

        
})
})




rout.post("/post", (req, res) => {  // user_name,email,mobile_number
    data = req.body;
    keyList = ["user_name", "email", "mobile_number"]
    default_key = []

    for (item of Object.keys(data))
    {
        if(keyList.includes(item))
        {
            continue
        }
        default_key.push(item)
        
    }

    if (default_key.length == 0) {

        if (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(data.email) == true & /^(\d{3})[- ]?(\d{3})[- ]?(\d{4})$/.test(data.mobile_number) == true) {
            console.log("insode if condition")
            qry = "insert into user(user_name,email,mobile_number) values (?,?,?)";
            lst = [data.user_name, data.email, data.mobile_number]
            cN.query(qry, lst, (err, result) => {

                if (err) {
                    return res.send(err)
                }
                return res.status(200).send("Data Entry is successfull")
                }
                )
        }

        else{
        return res.status(422).send("please enter valid email and mobile number")
        }
     }
    else{
        return res.status(422).send({ "not exist": default_key })
    } 
}
)


rout.put("/put/:user_id", (req, res) => {  // user_id ,user_name ,email ,mobile_number

    data = req.body;
    keyList = ["user_name", "email", "mobile_number"]
    default_key = []

    for (item of Object.keys(data))
    {
        if(keyList.includes(item))
        {
            continue
        }
        default_key.push(item)
        
    }

    if (default_key.length == 0) {



        if (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(data.email) == true & /^(\d{3})[- ]?(\d{3})[- ]?(\d{4})$/.test(data.mobile_number) == true) {
            


            qry = "update user set user_name=?,email=?,mobile_number=? where user_id=?";
            lst = [data.user_name, data.email, data.mobile_number,req.params.user_id]
            cN.query(qry, lst,(err, result) => {

                if (err) {
                return res.send(err)
                }
                return res.status(200).send("Data will be updated")
                
                
                
            }

            )
        }
        else{return res.status(422).send("please enter valid email and mobile number")}
        
        

    }
    else{
        return res.status(422).send({ "not exist": default_key })
    }





   

})


rout.delete("/delete/:user_id", (req, res) => {
    qry = "select * from user where user_id=?";
    cN.query(qry,req.params.user_id, (err, result) => {
        if (err) {
            res.send(err)
        }
        
        if(result.length==0)
        {
            return res.status(422).send("this user_id not present is database")
        }
 
        qry = "delete from user where user_id=?";
        cN.query(qry, req.params.user_id, (err, result) => {
            if (err) {
                return res.send(err)
            }
            return  res.send("Data  deleted")
            
        })


        
    })
})
//ss   s

module.exports = rout;