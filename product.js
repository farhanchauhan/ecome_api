
const cN = require("./connection");
const express = require("express");
const rout = express.Router();
rout.use(express.json())




rout.get("/get", (req, res) => {
    qry = `select * from product as p 
    inner join category as  c on p.category_id=c.category_id;
    `;
    cN.query(qry, (err, result) => {
        if (err) {
            return res.send(err)
        }
        console.log(result)
        let product_detail_list = []
        for (i of result)
        {
            let product_detail =
            {
                "product_id": i.product_id,
                "product_name": i.product_name,
                "product_image": i.product_image,
                "product_price": i.product_price,
                "product_discount": i.product_discount,
                "product_discount_type": i.product_discount_type,
                "product_discription": i.product_discription,
                "category_id": i.category_id,
                "category_name": i.category_name
            }
            if (i.product_discount_type == "percentage") {
                product_detail["product_total_amount_after_discount"]= i.product_price - (i.product_price * (i.product_discount / 100))
                product_detail_list.push(product_detail)
            }
            else{
                product_detail["product_total_amount_after_discount"]= i.product_price - i.product_discount
                product_detail_list.push(product_detail)
            }

        }

        return res.send(product_detail_list)
        
    }

    )
})





rout.get("/get/:product_id", (req, res) => {



    qry = `select * from product as p 
    inner join category as  c on p.category_id=c.category_id where product_id=?;
    `;
    cN.query(qry, req.params.product_id, (err, result) => {
        if (err) {
            return res.send(err)
        }

        if (result.length == 0) {
            return res.status(400).send("this id not present in database")
        }

        let product_detail =
        {
            "product_id": result[0].product_id,
            "product_name": result[0].product_name,
            "product_image": result[0].product_image,
            "product_price": result[0].product_price,
            "product_discount": result[0].product_discount,
            "product_discount_type": result[0].product_discount_type,
            "product_discription": result[0].product_discription,   
            "category_id": result[0].category_id,
            "category_name": result[0].category_name
        }

        if (result[0].product_discount_type == "percentage") {
            product_detail.product_total_amount_after_discount = result[0].product_price-(result[0].product_price*(result[0].product_discount/100))

        }
        else  {
            product_detail.product_total_amount_after_discount = result[0].product_price - result[0].product_discount

        }
        
       return res.send(product_detail)


    }

    )
})







rout.post("/post", (req, res) => {  //product_name,product_image,product_price,product_discount_type,product_discription,categories
    // data.product_image=LOAD_FILE('/path/to/your/image.jpg')
    //?,LOAD_FILE(?),?,?,?,?
    data = req.body;
    keyList=["product_name","product_image","product_price","product_discount","product_discount_type","product_discription","category_id"]
    default_key=[]
    for (item of Object.keys(data))
    {
        if(keyList.includes(item))
        {
            continue
        }
        default_key.push(item)
        
    }

    if (default_key.length == 0) {
        qry = `INSERT INTO product(product_name,product_image,product_price,product_discount,product_discount_type,product_discription,category_id)
        values(?,?,?,?,?,?,?)`;
        cN.query(qry, [data.product_name, data.product_image, data.product_price, data.product_discount, data.product_discount_type, data.product_discription, data.category_id], (err, result) => {

            if (err) {
                return res.send(err)
            }
            return res.status(200).send("Data Entry is successfull")
            
        }

        )
    }
    else {
        return res.status(422).send({ "not exist": default_key })
    }


})


rout.put("/put/:product_id", (req, res) => {
    // product_id,product_name,product_image,product_price,product_discount,product_discount_type,product_discription,categories_id



    qry = `select * from product where product_id=?`
    cN.query(qry,req.params.product_id, (err, result) => {
        if (err) {
            res.send(err)
        }
        else {

            if(result.length==0)
            {
                res.status(400).send("this product id not presend in data base")
            }

            else{
                data = req.body;
                keyList=["product_name","product_image","product_price","product_discount","product_discount_type","product_discription","category_id"]
                default_key=[]
                for (item of Object.keys(data))
                {
                    if(keyList.includes(item))
                    {
                        continue
                    }
                    default_key.push(item)
            
                }
                if (default_key.length == 0) {
                    qry = "update product set  product_name=?,product_image=?,product_price=?,product_discount=?,product_discount_type=?,product_discription=?,category_id=? where product_id=?";
                    cN.query(qry, [data.product_name, data.product_image, data.product_price, data.product_discount, data.product_discount_type, data.product_discription, data.category_id, req.params.product_id], (err, result) => {

                        if (err) {
                            return res.send(err)
                        }

                        else {
                           return  res.status(200).send("data will be updated")
                        }
                    }

                    )
                }
                else {
                    res.status(422).send({ "not exist": default_key })
                }





            }
        }
    })















})

rout.delete("/delete/:product_id", (req, res) => {



    qry = "select * from product where product_id=?";
    cN.query(qry, req.params.product_id,(err, result) => {
        if (err) {
            res.send(err)
        }
        else {
            if(result.length==0)
            {
                res.status(422).send("product_id not present id database")
            }
            else{qry = "delete from product where product_id=?";
            cN.query(qry, req.params.product_id, (err, result) => {
                if (err) {
                    res.send(err)
                }
                else {
                    res.send("data deleted")
                }
            })}
        }
    })
})






module.exports = rout;