create database ecommerce;
use ecommerce;
-- user table
create table user(
user_id int primary key not null auto_increment ,
user_name varchar(20),
email varchar(40),
mobile_number int
);

-- user category table
select * from user;
create table category(
category_id int primary key not null auto_increment,
category_name varchar(20) unique,
category_image varchar(200)
);

select * from category;
-- product table
create table product(
product_id int not null primary key,
product_name varchar(20),
product_image varchar(300),
product_price int,
product_discount int,
product_discount_type varchar(30),
product_discription varchar(100),
category_id int

);

select * from product;

-- cart table
create table cart(
-- cart_id,product_id,quantity
cart_id int primary key not null auto_increment,
product_id int,
user_id int,
quantity int

);

select * from cart;