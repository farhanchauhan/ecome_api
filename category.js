
const cN = require("./connection");
const express = require("express");
const rout = express.Router();
rout.use(express.json())

rout.get("/get", (req, res) => {
     qry = "select * from category";
     cN.query(qry, (err, result) => {
         if (err) {
             return res.send(err)
         }

         else {

             let category_detail_list=[]
             for (i=0;i<result.length;i++)
             {
                let category_detail=
                {
                    "category_id":result[i].category_id,
                    "category_name":result[i].category_name,
                    "category_image":result[i].category_image
                }
                category_detail_list.push(category_detail)
             }
             return res.status(200).send(category_detail_list)
         }
     }

     )
 })


 rout.get("/get/:category_id", (req, res) => {



    qry = `select * from category where category_id=? `
    cN.query(qry,req.params.category_id, (err, result) => {


        if (err) {
            return res.send(err)
        }
        if (result.length==0)

        {
            return res.status(400).send("please enter valid category_id")
        }

        let category_detail=
        {
            "category_id":result[0].category_id,
            "category_name":result[0].category_name,
            "category_image":result[0].category_image
        }

        return res.status(200).send(category_detail)
            

    })

})




rout.post("/post", (req, res) => {

    keyList=["category_name","category_image"]
    default_key=[]
    for (item of Object.keys(req.body))
    {
        if(keyList.includes(item))
        {
            continue
        }
        else{
            default_key.push(item)
        }
    }


    if(default_key.length==0)
    {
        data = req.body;
        qry = "insert into category(category_name,category_image) values (?,?)";
        cN.query(qry, [data.category_name, data.category_image], (err, result) => {
    
            if (err) {
    
                return res.send(err)
            }
            return res.status(200).send("Data Entry is successfu")
            
        }
    
        )


    }
    else{
        res.status(422).send({"not exist":default_key})
    }


})


rout.put("/put/:category_id", (req, res) => {  
    
    
    // category_id,category_name,category_description
    

    qry = `select * from category`
    cN.query(qry, (err, result) => {
        if (err) {
            res.send(err)
        }
        else {
            
            if(result.length==0)
            {
                res.status(422).send("please enter valid category_id")
            }
            else{
                keyList=["category_name","category_image"]
                default_key=[]
                for (item of Object.keys(req.body))
                {
                    if(keyList.includes(item))
                    {
                        continue
                    }
                    default_key.push(item)
                }

            
                
                if(default_key.length==0){
                    data = req.body;
                    qry = "update category set  category_name=?,category_image=? where category_id=?";
                    cN.query(qry, [data.category_name, data.category_image, req.params.category_id], (err, result) => {
                
                        if (err) {
                            return res.send(err)
                        }
                        return res.status(200).send("Data will be updated")
                        
                    }
                
                    )
            
                }
                else{
                    res.status(422).send({"not exist":default_key})
                }
            }
            }


        })


})




rout.delete("/delete/:category_id", (req, res) => {
    qry = "select * from category where category_id=?";
    cN.query(qry,req.params.category_id, (err, result) => {
        if (err) {
            res.send(err)
        }

        if(result.length==0)
        {
            return res.status(422).send("this category id not present in database")
        }

        qry = "delete from category where category_id=?";
        cN.query(qry, req.params.category_id, (err, result) => {
            if (err) {
                return res.send(err)
            }
            return res.send("Data  deleted")
            
        })

        
    })
})





module.exports = rout;