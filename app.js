const express=require("express")
const app=express();
const userRouter=require("./user.js")
const productRouter=require("./product.js")
const categoryRouter=require("./category.js")
const cartRouter=require("./cart.js")

app.use("/user",userRouter)
app.use("/product",productRouter)
app.use("/category",categoryRouter)
app.use("/cart",cartRouter)



app.listen(3000);