
const cN = require("./connection");
const express = require("express");
const rout = express.Router();
rout.use(express.json())

rout.get("/get/:user_id", (req, res) => {

    qry = `select u.user_id,u.user_name,p.product_name,p.product_image,p.product_price,p.product_discount,p.product_discount_type,p.product_discription,c.quantity from ecommerce.cart as c
    left join ecommerce.user as u on c.user_id=u.user_id
    left join ecommerce.product as p on p.product_id =c.product_id
    where u.user_id=?`;
    cN.query(qry, req.params.user_id, (err, result) => {

        if (err) {
            return res.send(err)
        }

        if (result.length == 0) {
            return res.status(404).send("user id not present in database")

        }
        cart_items_list = []
        total_price = 0

        for (i of result) {
            let cart_items =
            {
                product_name: i.product_name,
                "product_image": i.product_image,
                "product_price": i.product_price,
                "product_discount": i.product_discount,
                "product_discount_type": i.product_discount_type,
                "product_discription": i.product_discription,
                "quantity": i.quantity,

            }

            if (i.product_discount_type == "percentage") {
                cart_items["total_amount"] = (i.product_price - (i.product_price * (i.product_discount / 100))) * i.quantity
                cart_items_list.push(cart_items)
                total_price += (i.product_price - (i.product_price * (i.product_discount / 100))) * i.quantity

            }
            else{
                cart_items["total_amount"] = i.product_price - i.product_discount
            cart_items_list.push(cart_items)
            total_price += i.product_price - i.product_discount

            }
            
        }

        res.status(200).send({ user_name: result[0].user_name, total_product: cart_items_list.length, product_list: cart_items_list, total_amount: total_price })

    }

    )
}

)


rout.post("/post", (req, res) => {
    data = req.body;
    keyList = ["user_id", "product_id", "quantity"]
    default_key = []

    for (item of Object.keys(data)) {
        if (keyList.includes(item)) {
            continue
        }
        default_key.push(item)
    }
    if (default_key.length == 0) {
        qry = "INSERT INTO cart(user_id,product_id,quantity) values(?,?,?)";
        cN.query(qry, [data.user_id, data.product_id, data.quantity], (err, result) => {

            if (err) {
                return res.send(err)
            }
            res.status(200).send("data netry is success full")
        }

        )
    }
    else{res.status(422).send({ "not exist": default_key })}

    

})


rout.put("/put/:cart_id", (req, res) => {  // cart_id,product_id,quantity

    cN.query("select * from cart where cart_id=?",req.params.cart_id,(err,result)=>
    {
        if(err)
        {
            res.send(err)
        }
        else{
            console.log(result)
            if(result.length==0)
            {
                res.status(422).send("this cart_id not present in database")
            }
            else{

                data = req.body;
                keyList = ["user_id", "product_id", "quantity"]
                default_key = []
            
                for (item of Object.keys(data)) {
                    if (keyList.includes(item)) {
                        continue;
                    }
                    default_key.push(item)
            
                }
            
            
                if (default_key.length == 0) {
                    qry = "update cart set  user_id=?, product_id=?,quantity=? where cart_id=?";
                    cN.query(qry, [data.user_id, data.product_id, data.quantity, req.params.cart_id], (err, result) => {
            
                        if (err) {
                            res.send(err)
                        }
            
                        res.status(200).send("Data will be updated")
            
                    }
            
                    )
            
            
                }
                else{

                    res.status(400).send({ "not exist": default_key })
                }
            
                

            }
 
        
        }
    })

   

})


// rout.delete("/delete/:cart_id", (req, res) => {  // user_id ,user_name ,email ,mobile_number

//     qry = "delete from cart where cart_id=?";
//     cN.query(qry, req.params.cart_id, (err, result) => {

//         if (err) {
//             res.send(err)
//         }

//         else {
//             res.send(result)
//         }
//     }

//     )
// })





rout.delete("/delete/:cart_id", (req, res) => {

    qry = "select * from cart where cart_id=?";
    cN.query(qry,req.params.cart_id, (err, result) => {
        if (err) {
           return  res.send(err)
        }
        if(result.length==0)
        {
            res.status(400).send("this cart_id note present in database")
        }

        else{
            cN.query("delete from cart where cart_id=?",req.params.cart_id,(err,result)=>
            {
                if(err)
                {
                   return  res.status(400).send(err)
                }
                return res.status(200).send("data will be deleted")
            })


        }


        
    })



})



module.exports = rout;